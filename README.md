# mparch

This isn't a different flavour of arch linux, it's an install script for tools commonly used
by penetration testers. All the packages that this script installs is listed in ``packages.json``.

It was developed specifically for arch linux and for use inside F-Secure Consulting.

## Installation

To install the normal packages run:

````sh
sudo ./install.sh
````

To install the aur packages run:

````sh
./install.sh -a
````

To install all the packes, run the following command from a user other than root:

````
sudo ./install.sh; ./install.sh -a
````

The last command makes it harder to see what went wrong, it's better to run the commands separately and ensure that
all the packages installed successfully.

## Out Of Date Packages

- frida