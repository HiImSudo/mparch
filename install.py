#!/usr/bin/env python3

import subprocess
import json
import argparse
import pprint

parser = argparse.ArgumentParser(description="Install common pentesting packages")
group = parser.add_mutually_exclusive_group()
group.add_argument("-p", dest="package", default="./packages.json", help="Specify a list of packages in a json file.")
group.add_argument("-a", dest="aur", action="store_true", help="Also install aur packages.")
group.add_argument("-q", dest="quiet", action="store_true", help="Suppress all pacman/yay output.") # TODO
args = parser.parse_args()


packages = None
with open(args.package, 'r') as fd:
    packages = json.load(fd)

failures = {"aur": [], "arch": []}
arch_packages_sets = packages['arch']
aur_packages_sets = packages['aur']

if not args.aur:
    for package_set in arch_packages_sets.values():
        for package in package_set:
            result = subprocess.run(['pacman', '-S', package, '--noconfirm'])
            if result.returncode != 0:
                failures["arch"].append(package)

if args.aur:
    for package_set in aur_packages_sets.values():
        for package in package_set:
            result = subprocess.run(['yay', '-S', package, '-a'])
            if result.returncode != 0:
                failures["aur"].append(package)

if packages.get('custom') != None:
    for key in packages['custom'].keys():
        for command in packages['custom'][key]:
                result = subprocess.run(command.split(' '))
                if result.returncode != 0:
                    failures["custom"].append(command)


if len(failures["aur"]) == len(failures["arch"]):
    print("All packages installed")
else:
    print("The following packages failed to install:")
    pprint.pprint(failures)
